const createResponse = (statusCode, payload) => {
  var res = {
    "statusCode": parseInt(statusCode),
    "headers": {
      "Access-Control-Allow-Origin": "*"
    },
    "body": JSON.stringify(payload)
  }
  return res
}


class LambdaLogger {
  constructor(category) {
    this.category = category
    this.event = null
    this.context = null
  }

  convertLogToJson(level, msg) {
    return {
      category: this.category,
      level: level,
      msg: msg,
      event: this.event,
      context: this.context
    }
  }

  debugLambdaStart(event, context) {
    if (event) this.event = (typeof event === 'string') ? event : JSON.stringify(event)

    if (context) this.context = context

    this.debug("Lambda Start")
  }

  log(msg) {
    console.log(this.convertLogToJson('log', msg))
  }

  debug(msg) {
    console.log(this.convertLogToJson('debug', msg))
  }

  info(msg) {
    console.log(this.convertLogToJson('info', msg))
  }

  warn(msg) {
    console.log(this.convertLogToJson('warn', msg))
  }

  error(msg) {
    console.log(this.convertLogToJson('error', msg))
  }

}

module.exports.LambdaLogger = LambdaLogger
module.exports.createResponse = createResponse
